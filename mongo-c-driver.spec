%global with_tests   0%{?_with_tests:1}
Name:          mongo-c-driver
Summary:       Client library written in C for MongoDB
Version:       1.13.1
Release:       5 
License:       ASL 2.0 and ISC and MIT and zlib
URL:           https://github.com/mongodb/mongo-c-driver
Source0:       https://github.com/mongodb/mongo-c-driver/releases/download/1.13.1/mongo-c-driver-1.13.1.tar.gz
BuildRequires: cmake >= 3.1 openssl-devel pkgconfig(libsasl2) pkgconfig(zlib) gcc
BuildRequires: pkgconfig(snappy) pkgconfig(icu-uc) perl-interpreter python3 python3-sphinx
%if %{with_tests}
BuildRequires: mongodb-server openssl
%endif
Obsoletes:     mongo-c-driver-tools < 1.3.0 mongo-c-driver-libs < %{version}-%{release}
Provides:      mongo-c-driver-tools = 1.13.1 mongo-c-driver-tools%{?_isa} = 1.13.1
Provides:      mongo-c-driver-libs = %{version}-%{release} mongo-c-driver-libs%{?_isa} = %{version}-%{release}

%description
mongo-c-driver is a project that includes two libraries: libmongoc, a client library
written in C for MongoDB. libbson, a library providing useful routines related to
building, parsing, and iterating BSON documents.

%package devel
Summary:       Header files and development libraries for %{name}
Requires:      mongo-c-driver = %{version}-%{release} pkgconfig cmake-filesystem

%description devel
The mongo-c-driver-devel package includes header files and libraries necessary
for the mongo-c-driver library.

%package -n libbson
Summary:       Building, parsing, and iterating BSON documents
Provides:      bundled(jsonsl)

%description -n libbson
mongo-c-driver is a project that includes two libraries: libmongoc, a client library
written in C for MongoDB. libbson, a library providing useful routines related to
building, parsing, and iterating BSON documents.

%package -n libbson-devel
Summary:       Development files for mongo-c-driver
Requires:      libbson = %{version}-%{release} pkgconfig cmake-filesystem

%description -n libbson-devel
The libbson-devel package includes header files and libraries necessary
for the libbson library.

%package help
Summary:       This package contains help documents
Requires:      %{name} = %{version}-%{release}

%description help
Files for help with mongo-c-driver.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake -DENABLE_BSON:STRING=ON -DENABLE_MONGOC:BOOL=ON -DENABLE_SHM_COUNTERS:BOOL=ON \
    -DENABLE_SSL:STRING=OPENSSL -DENABLE_SASL:STRING=CYRUS -DENABLE_ICU:STRING=ON \
    -DENABLE_AUTOMATIC_INIT_AND_CLEANUP:BOOL=OFF -DENABLE_CRYPTO_SYSTEM_PROFILE:BOOL=ON \
    -DENABLE_MAN_PAGES:BOOL=ON -DENABLE_TESTS:BOOL=ON -DENABLE_EXAMPLES:BOOL=OFF \
    -DENABLE_UNINSTALL:BOOL=OFF .
%make_build

%install
%make_install
%delete_la_and_a
find %{buildroot}%{_libdir} -type f -name "*static*" -exec rm -f {} ';'

%check
%if %{with_tests}
install -d dbtest
mongod --journal --ipv6 --unixSocketPrefix /tmp --logpath $PWD/server.log \
  --pidfilepath $PWD/server.pid --dbpath $PWD/dbtest --fork
ret=0
export MONGOC_TEST_OFFLINE=on
make check || ret=1
[ -s server.pid ] && kill $(cat server.pid)
exit $ret
%endif

%files
%{_bindir}/mongoc-stat
%doc COPYING
%doc THIRD_PARTY_NOTICES
%{_libdir}/libmongoc-1.0.so.*

%files devel
%doc src/libmongoc/examples
%{_includedir}/libmongoc-1.0
%{_libdir}/libmongoc-1.0.so
%{_libdir}/pkgconfig/libmongoc-*.pc
%{_libdir}/cmake/libmongoc-1.0

%files -n libbson
%{_libdir}/libbson*.so.*

%files -n libbson-devel
%doc src/libbson/examples
%{_includedir}/libbson-1.0
%{_libdir}/libbson*.so
%{_libdir}/cmake/libbson-1.0
%{_libdir}/pkgconfig/libbson-*.pc

%files help
%{_mandir}/man3/*
%doc src/libbson/NEWS
%doc NEWS

%changelog
* Web 02 Jun 2021 zhaoyao<zhaoyao32@huawei.com> - 1.13.1-5
- Add gcc build require.

* Fri Jun 19 2020 sunguoshuai<sunguoshuai@huawei.com> - 1.13.1-4
- Use python3 instead of python2.

* Fri Feb 21 2020 gulining<gulining1@huawei.com> - 1.13.1-3
- fix build error

* Wed Jan 8 2020 gulining<gulining1@huawei.com> - 1.13.1-2
- Pakcage init
